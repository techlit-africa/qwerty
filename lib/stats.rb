class Stats
  attr_accessor :exercises_by_id, :users_by_id

  def initialize
    @exercises_by_id = {}
    @users_by_id = {}

    user_exercises.each do |ue|
      user(ue)
      exercise(ue)
    end

    users.each do |u|
      id = u['id'].to_i

      x = @users_by_id[id] || new_user(id, u['username'])

      x[:avg_wpm] = u['avg_wpm']
      x[:avg_accuracy] = u['avg_accuracy']
      x[:max_wpm] = u['max_wpm']
      x[:max_accuracy] = u['max_accuracy']

      @users_by_id[id] = x
    end

    @users_by_id.values.each do |u|
      u[:lesson], u[:exercise] = Exercise.next_positions(u[:lesson], u[:exercise])
      u[:position] = position(u[:lesson], u[:exercise])
      u[:next_exercise_id] = Exercise.next_exercise_id(u[:lesson_id], u[:exercise_id])
    end

    @progress = @users_by_id.values.sort_by{_1[:position]}.reverse.take(10)
    @wpm = @users_by_id.values.sort_by{_1[:max_wpm]}.reverse.take(10).select{_1[:max_wpm] > 0}
    @star = @users_by_id.values.sort_by{_1[:star]}.reverse.take(10).select{_1[:star] > 0}
  end

  def global
    {
      users: @users_by_id,
      exercises: @exercises_by_id,
      total_badges: Exercise.all_by_id.length,
      progress: @progress,
      wpm: @wpm,
      star: @star,
      first_exercise_id: Lesson.for_home_page.dig(0, :exercises, 0, :id),
    }
  end

  private

  def first_lesson_id
    @first_lesson_id ||= Lesson.first_id
  end

  def first_exercise_id
    @first_exercise_id ||= Exercise.first_id
  end

  def new_user(id, username)
    {
      id: id,
      username: username,
      position: -1,
      lesson: 0, exercise: -1,
      lesson_id: first_lesson_id, exercise_id: first_exercise_id,
      complete: 0, accurate: 0, fast: 0, star: 0,
      avg_wpm: 0, avg_accuracy: 0, max_wpm: 0, max_accuracy: 0,
    }
  end

  def new_exercise(id, ue)
    {
      id: id,
      title: ue['exercise_title'],
      position: ue['exercise_position'],
      lesson_position: ue['lesson_position'],
      lesson_title: ue['lesson_title'],
      complete: 0, accurate: 0, fast: 0, star: 0,
    }
  end

  def user(ue)
    id = ue['user_id'].to_i

    u = @users_by_id[id] || new_user(id, ue['username'])

    u[:complete] += 1
    u[:accurate] += ue['accurate']
    u[:fast] += ue['fast']
    u[:star] += (ue['accurate'] + ue['fast'] > 1 ? 1 : 0)

    @users_by_id[id] = u

    lp, ep = ue['lesson_position'], ue['exercise_position']
    pos = position(lp, ep)
    if u[:position] < pos
      u[:position], u[:lesson], u[:exercise] = pos, lp, ep
      u[:lesson_id], u[:exercise_id] = ue['lesson_id'], ue['exercise_id']
    end

    id
  end

  def position(lesson_position, exercise_position)
    (lesson_position * 100) + exercise_position
  end

  def exercise(ue)
    id = ue['exercise_id']

    e = @exercises_by_id[id] || new_exercise(id, ue)

    e[:complete] += 1
    e[:accurate] += ue['accurate']
    e[:fast] += ue['fast']
    e[:star] += (ue['accurate'] + ue['fast'] > 1 ? 1 : 0)

    @exercises_by_id[id] = e

    id
  end

  def execute(...) = ApplicationRecord.connection.execute(...)

  def user_exercises
    @user_exercises ||= execute(<<~SQL)
      select
        u.id as user_id,
        l.id as lesson_id,
        e.id as exercise_id,
        u.klass,
        u.username,
        l.title as lesson_title,
        l.position as lesson_position,
        e.title as exercise_title,
        e.position as exercise_position,
        max(s.accurate) as accurate,
        max(s.fast) as fast,
        max(s.wpm) as max_wpm,
        max(s.accuracy) as max_accuracy,
        s.created_at
      from
        submissions s
        join exercises e on e.id = s.exercise_id
        join lessons l on l.id = e.lesson_id
        join users u on u.id = s.user_id
      where
        s.complete
      group by
        user_id, exercise_id
    SQL
  end

  def users
    @users ||= execute(<<~SQL)
      select
        u.id,
        u.username,
        avg(s.wpm) as avg_wpm,
        max(s.wpm) as max_wpm,
        avg(s.accuracy) as avg_accuracy,
        max(s.accuracy) as max_accuracy
      from
        users u
        join test_submissions s on s.user_id = u.id
      group by
        user_id
    SQL
  end
end
