Rails.application.routes.draw do
  scope '/api' do
    resources :users, only: %i[index create] do
      post :me, action: :update
    end

    resources :sessions, only: %i[create]

    resources :lessons, only: %i[index]
    resources :exercises, only: %i[show]
    resources :submissions, only: %i[create show]
    resources :test_submissions, only: %i[create show]
    resources :stats, only: %i[index]

    match '*path', to: 'application#not_found', via: %i[get post put patch delete]
  end

  get '*path', to: 'application#client_html', constraints: -> (req) do
    !req.xhr? && req.format.html?
  end
end
