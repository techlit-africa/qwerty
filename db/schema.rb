# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_02_10_161937) do
  create_table "courses", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "cuid"
    t.string "title"
    t.string "version"
  end

  create_table "exercises", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "lesson_id"
    t.string "cuid"
    t.integer "position"
    t.string "title"
    t.string "content"
    t.integer "okay_typos"
    t.integer "okay_wpm"
    t.integer "good_typos"
    t.integer "good_wpm"
    t.index ["lesson_id"], name: "index_exercises_on_lesson_id"
  end

  create_table "lessons", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "course_id"
    t.string "cuid"
    t.integer "position"
    t.string "title"
    t.index ["course_id"], name: "index_lessons_on_course_id"
  end

  create_table "submissions", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "exercise_id"
    t.integer "user_id"
    t.integer "wpm"
    t.integer "typos"
    t.decimal "accuracy"
    t.boolean "complete", default: false
    t.boolean "accurate", default: false
    t.boolean "fast", default: false
    t.index ["exercise_id"], name: "index_submissions_on_exercise_id"
    t.index ["user_id"], name: "index_submissions_on_user_id"
  end

  create_table "test_submissions", force: :cascade do |t|
    t.integer "user_id"
    t.text "content"
    t.integer "wpm"
    t.integer "typos"
    t.decimal "accuracy"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_test_submissions_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "klass"
    t.string "username"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
