class CreateCourses < ActiveRecord::Migration[7.0]
  def change
    create_table :courses do |t|
      t.timestamps
      t.string :cuid
      t.string :title
      t.string :version
    end

    create_table :lessons do |t|
      t.timestamps
      t.references :course
      t.string :cuid
      t.integer :position
      t.string :title
    end

    create_table :exercises do |t|
      t.timestamps
      t.references :lesson
      t.string :cuid
      t.integer :position
      t.string :title
      t.string :content
      t.integer :okay_typos
      t.integer :okay_wpm
      t.integer :good_typos
      t.integer :good_wpm
    end

    create_table :submissions do |t|
      t.timestamps
      t.references :exercise
      t.references :user
      t.integer :wpm
      t.integer :typos
      t.decimal :accuracy
      t.boolean :complete, default: false
      t.boolean :accurate, default: false
      t.boolean :fast, default: false
    end

    create_table :test_submissions do |t|
      t.references :user
      t.text :content
      t.integer :wpm
      t.integer :typos
      t.decimal :accuracy
      t.timestamps
    end
  end
end
