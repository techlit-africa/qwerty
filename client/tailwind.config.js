module.exports = {
  separator: '_',
  purge: ['./**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class',
  variants: {
    extend: {
      display: ['dark'],
      backgroundColor: ['disabled'],
    },
  },
  plugins: [],
}
