const {
  override,
  disableEsLint,
  addBabelPlugins,
  addPostcssPlugins,
  addWebpackPlugin,
} = require('customize-cra')

const webpack = require('webpack')

module.exports = override(
  disableEsLint(),

  ...addBabelPlugins(
    'transform-jsx-classname-components',
    'transform-react-pug',
  ),

  addPostcssPlugins([
    require('autoprefixer'),
    require('tailwindcss'),
  ]),

  addWebpackPlugin(new webpack.ProvidePlugin({
    React: 'react',
    ErrorBoundary: ['react-error-boundary', 'ErrorBoundary'],
    Icon: ['@fortawesome/react-fontawesome', 'FontAwesomeIcon'],
  })),

  addWebpackPlugin(new webpack.DefinePlugin({
    NODE_ENV: `"${process.env.NODE_ENV || 'development'}"`,
  })),
)
