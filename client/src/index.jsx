import {render} from 'react-dom'
import {BrowserRouter} from 'react-router-dom'
import {FetchProvider} from 'lib/fetch'
import {SWRProvider} from 'lib/swr'
import {SoundsProvider} from 'hooks/useSounds'
import {App} from 'components/App'
import {syncLightsOn} from 'lib/theme'

import 'styles/index.css'
import 'init/fontawesome'
import 'init/dayjs'

const Main = () => pug`
  - syncLightsOn()

  React.StrictMode
    SoundsProvider
      BrowserRouter
        FetchProvider
          SWRProvider
            App
`

render(pug`Main`, document.getElementById('root'))
