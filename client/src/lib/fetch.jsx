import {provideContext} from 'lib/provideContext'

const getUserFromStorage = () => {
  try { return JSON.parse(localStorage.getItem('techlit.qwerty.user')) }
  catch (e) { return null }
}

const setUserInStorage = (user = null) => {
  localStorage.setItem('techlit.qwerty.user', JSON.stringify(user))
}

export const {Provider: FetchProvider, useContext: useFetch} = provideContext(() => {
  const [user, setUser] = React.useState(getUserFromStorage())
  React.useEffect(() => setUserInStorage(user), [user])

  const fetch = React.useCallback(async (path, ops = {}) => {
    const rep = await window.fetch(`/api/${path}`, {
      ...ops,
      headers: {
        'Content-Type': 'application/json',
        ...(user ? {'Authorization': `Bearer ${user.token}`} : null),
        ...(ops.headers || {}),
      },
    })
    if (rep.status == 401) return setUser(null)
    return await rep.json()
  }, [user])

  return {fetch, user, setUser}
})
