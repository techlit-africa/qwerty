import {SWRConfig} from 'swr'
import {useFetch} from 'lib/fetch'

export const SWRProvider = ({children}) => {
  const {fetch} = useFetch()

  const swrConfig = {
    fetcher: fetch,
    suspense: true,
    revalidateOnMount: true,
    dedupingInterval: 100,
    focusThrottleInterval: 100,
    refreshInterval: 120000,
  }

  return pug`
    SWRConfig(value=swrConfig) #{children}
  `
}
