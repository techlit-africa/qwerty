import {Layout, VisitorRight} from 'components/Layout'
import {Input} from 'components/Input'
import {Loading} from 'components/Loading'
import {Failed} from 'components/Failed'
import {useFetch} from 'lib/fetch'
import {useHistory, Link} from 'react-router-dom'
import fuzzysearch from 'fuzzysearch'
import useSWR from 'swr'

export const Login = () => pug`
  Layout(left=${pug`.w-60`} title='Login' right=${pug`VisitorRight`})
    .w-full.flex.justify-center.p-16
      .w-full.max-w-sm.border.border-gray-500.bg-gray-100.dark_bg-gray-900.shadow.rounded
        .p-4
          ErrorBoundary(FallbackComponent=Failed)
            React.Suspense(fallback=${pug`Loading`})
              Ready
`

const useLoginForm = () => {
  const {data: users} = useSWR('/users')
  const {fetch, setUser} = useFetch()
  const history = useHistory()

  const [state, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {
      step: 'findingUser', // findingUser | enterPassword | loading
      offset: 0,
      error: null,
      newAccount: false,

      klass: '',
      username: '',
      password: '',
    }
  )

  const matchingUsers = React.useMemo(
    () => users.filter((user) => {
      if (state.klass != '' && !fuzzysearch(state.klass.trim().toLowerCase(), user.klass)) return false
      if (state.username != '' && !fuzzysearch(state.username.trim().toLowerCase(), user.username)) return false
      return true
    }),
    [users, state.klass, state.username],
  )

  const onChange = (propName) => (e) => update({
    [propName]: e.target.value,
    error: null
  })
  const onUsernameChange = (e) => update({
    username: e.target.value.replaceAll(/[^a-zA-Z0-9-]+/g, '-').toLowerCase(),
    error: null,
  })

  const pickUser = ({klass, username}) => {
    update({
      step: 'enteringPassword',
      klass,
      username,
      newAccount: false,
      error: null,
    })
    setTimeout(() => document.getElementById(PASSWORD_ID).focus(), 100)
  }

  const startAccount = () => update({
    step: 'enteringPassword',
    newAccount: true,
    error: null,
  })

  const unpickUser = () => {
    update({
      step: 'findingUser',
      klass: '',
      username: '',
      password: '',
      newAccount: false,
      error: null,
    })
    setTimeout(() => document.getElementById(CLASS_ID).focus(), 100)
  }

  const login = () => {
    update({step: 'loading'})

    const path = state.newAccount ? '/users' : '/sessions'
    const body = state.newAccount
      ? {klass: state.klass, username: state.username, password: state.password}
      : {username: state.username, password: state.password}

    fetch(path, {method: 'POST', body: JSON.stringify(body)})
      .then(({error, ...user}) => {
        if (error) return update({step: 'enteringPassword', error})
        if (!user?.token) return update({step: 'enteringPassword', error: 'request failed'})
        setUser(user)
        setTimeout(() => history.push('/stats'), 100)
      })
      .catch((error) => update({step: 'enteringPassword', error}))
  }

  React.useEffect(() => {
    setTimeout(() => document.getElementById(CLASS_ID).focus(), 100)
  }, [])

  return {
    ...state,
    update,
    onChange,
    onUsernameChange,
    pickUser,
    unpickUser,
    startAccount,
    login,
    matchingUsers,
  }
}

const Ready = () => pug`
  - const form = useLoginForm()

  if form.step == 'enteringPassword'
    button.text-blue-500.dark_text-blue-400(onClick=()=>form.unpickUser())
      Icon(icon='arrow-left')
      span.pl-2 Go back
    .h-3

  .grid.grid-cols-4.gap-3
    Input(
      wrapperClassName='col-span-1'
      id=CLASS_ID
      label='Class'
      placeholder='#'
      value=form.klass
      onChange=form.onChange('klass')
    )

    Input.font-mono(
      wrapperClassName='col-span-3'
      id=USERNAME_ID
      label='Username'
      placeholder='my-name'
      value=form.username
      onChange=form.onUsernameChange
    )

    if form.step != 'findingUser'
      Input(
        wrapperClassName='col-span-4'
        id=PASSWORD_ID
        label='Password'
        type='password'
        value=form.password
        onChange=form.onChange('password')
      )

      if form.error
        .col-span-4.text-red-500= form.error
      .h-1
      button.col-span-4.py-2.px-4.text-2xl.bg-blue-500.dark_bg-blue-400.disabled_bg-gray-500.text-white.dark_text-black.rounded.shadow-lg(
        onClick=form.login
        disabled=(form.step == 'loading')
      )= form.newAccount ? 'Create account' : 'Login'

  if form.step == 'findingUser'
    .w-full.h-0.border-b.border-gray-500.my-4

    if form.klass != '' || form.username != ''
      for user in form.matchingUsers.slice(0, 5)
        button.flex.justify-between.w-full.mb-2.py-2.px-4.text-left.border.border-gray-500.bg-gray-50.dark_bg-gray-900.text-gray-800.dark_text-gray-200.rounded.shadow(
          key=user.id
          onClick=()=>form.pickUser(user)
        )
          .text-xl.font-mono= user.username
          span class #{user.klass}

  if form.step == 'findingUser'
    .h-3
    .w-full.text-right
      button.text-blue-500.dark_text-blue-400(onClick=form.startAccount)
        Icon(icon='plus')
        span.pl-2 New account
`

const CLASS_ID = 'class-field'
const USERNAME_ID = 'username-field'
const PASSWORD_ID = 'password-field'
