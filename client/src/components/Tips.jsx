import {tips} from 'tips'
import {HomeLayout} from 'components/HomeLayout'

const useTip = () => {
  const x = tips.length * Math.random() | 0
  const [idx, setIdx] = React.useState(x)
  const [title, body] = tips[idx]

  const nextTip = () => setIdx(idx + 1 >= tips.length ? 0 : idx + 1)

  return {title, body, nextTip}
}

export const Tips = () => pug`
  - const {title, body, nextTip} = useTip()

  HomeLayout
    .max-w-screen-sm.mx-auto.flex.flex-col.items-center
      Icon.text-8xl.text-yellow-500(icon='hand-sparkles')
      .h-2
      .text-4xl.text-gray-700.dark_text-gray-300= title
      .h-2
      .text-xl.text-gray-800.dark_text-gray-200= body
      .h-4
      button.text-blue-500.dark_text-blue-400.text-2xl(onClick=nextTip) Next
`
