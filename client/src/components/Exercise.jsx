import {Layout, BackLeft, UserRight} from 'components/Layout'
import {Loading} from 'components/Loading'
import {Failed} from 'components/Failed'
import {Keyboard} from 'components/Keyboard'
import {useKeysDown} from 'hooks/useKeysDown'
import {useSounds} from 'hooks/useSounds'
import {Link, useParams, useHistory} from 'react-router-dom'
import {useFetch} from 'lib/fetch'
import useSwr from 'swr'

const loading = pug`
  Layout(left=${pug`BackLeft`} title='...' right=${pug`UserRight`}) #[Loading]
`

const failed = pug`
  Layout(left=${pug`BackLeft`} title='...' right=${pug`UserRight`}) #[Failed]
`

const ReadyTitle = ({exercise}) => pug`
  span.flex.flex-row.items-center.justify-center
    span.pr-3.text-base.text-gray-600.dark_text-gray-400.truncate #{exercise.lesson_title}:#{' '}
    span.text-gray-900.dark_text-gray-100.truncate= exercise.title
`

export const Exercise = () => pug`
  ErrorBoundary(fallback=failed)
    React.Suspense(fallback=loading)
      Ready
`

const exerciseInit = {
  wpm: 0,
  typos: 0,
  wrong: false,
  cursor: 0,
  ready: false,
  startedAt: false
}

const Ready = () => {
  const {id} = useParams()
  const history = useHistory()
  const {fetch} = useFetch()
  const {data: exercise} = useSwr(id ? `/exercises/${id}` : null)
  exercise.content = exercise.content

  const [{
    wpm,
    typos,
    wrong,
    cursor,
    ready,
    startedAt,
    stoppedAt,
    statsVisible,
    fingersVisible,
    keyboardVisible,
    showEscModal,
  }, reducer] =
    React.useReducer(
      (state, {type, data}) => ({
        update: (newState) => ({...state, ...newState}),
        wpmTick: () => {
          if (!state.startedAt || state.stoppedAt) return state
          const words = (state.cursor + 1) / 5
          const minutes = (new Date() - state.startedAt) / 1000 / 60
          return {
            ...state,
            wpm: Math.round(words / minutes),
          }
        },
      })[type]?.(data) || state,
      {
        ...exerciseInit,
        statsVisible: true,
        fingersVisible: true,
        keyboardVisible: true,
        showEscModal: false,
      },
    )

  const update = (s2) => reducer({type: 'update', data: s2})

  const {
    playOops,
    playKeyUp,
    playKeyDown,
    playStart,
    playEnd,
  } = useSounds()

  const restart = () => {
    playStart()
    update(exerciseInit)

    // TODO: add a UI effect that makes a 1000ms timeout less surprising
    setTimeout(() => update({ready: true}), 10)
  }

  React.useEffect(() => {
    restart()
    const wpmTick = setInterval(() => reducer({type: 'wpmTick'}), 300)
    return () => clearInterval(wpmTick)
  }, [])

  const keyDownRef = React.useRef()
  const keyUpRef = React.useRef()

  keyDownRef.current = (event) => {
    if (!ready || event.ctrlKey || event.altKey) return

    if (
      event.code.startsWith('Key') ||
      event.code.startsWith('Digit') ||
      [
        'Backquote',
        'Space',
        'Backspace',
        'Backslash',
        'BracketRight',
        'BracketLeft',
        'Quote',
        'Semicolon',
        'Slash',
        'Period',
        'Comma',
      ].includes(event.code)
    ) {
      event.stopPropagation()
      event.preventDefault()
    }

    playKeyDown()

    if (event.code === 'Escape') {
      event.preventDefault()
      update({showEscModal: !showEscModal})
      return
    }

    if (stoppedAt) return

    if (event.key === exercise.content[cursor]) {
      update({
        cursor: cursor + 1,
        wrong: false,
      })

      if (!startedAt) update({startedAt: new Date()})

      if (cursor >= exercise.content.length - 1) {
        update({stoppedAt: new Date()})

        playEnd()

        const length = exercise.content.length
        const accuracy = (length - typos) / length

        fetch('/submissions', {
          method: 'POST',
          body: JSON.stringify({exercise_id: id, typos, wpm, accuracy}),
        })
          .then(({error, ...submission}) => {
            if (error) {
              console.error(error)
              history.push(`/exercises/${id}`)
            }
            history.push(`/submissions/${submission.id}`)
          })
          .catch((error) => {
            console.error(error)
            history.push(`/exercises/${id}`)
          })
      }
      return
    }

    if (
      event.ctrlKey ||
      event.altKey ||
      ['ShiftLeft', 'ShiftRight', 'CapsLock'].includes(event.code)
    ) return

    update({wrong: true, typos: typos + 1})
    playOops()
  }

  keyUpRef.current = (event) => {
    playKeyUp()
  }

  const [shift, capsLock, keysDown] = useKeysDown({
    onKeyDownRef: keyDownRef,
    onKeyUpRef: keyUpRef,
  })

  return pug`
    Layout(left=${pug`BackLeft`} title=${pug`ReadyTitle(exercise=exercise)`} right=${pug`UserRight`})
      .w-full.flex-1.flex.flex-col.items-center
        .container.mx-auto.w-full.h-24.flex.flex-row.items-center.justify-between
          .w-64.h-full.pt-8
            span.flex.flex-row.items-center.justify-end
              span.pr-3.text-base.text-gray-600.dark_text-gray-400 Lesson
              span.pr-6.text-4xl.font-medium.text-gray-900.dark_text-gray-100= exercise.lesson_position + 1
              span.pr-3.text-base.text-gray-600.dark_text-gray-400 Exercise
              span.text-4xl.font-medium.text-gray-900.dark_text-gray-100= exercise.position + 1

          if statsVisible
            .flex-1.h-full.flex.flex-row.items-end.justify-center
              .flex.flex-col.items-center.px-8
                .text-4xl.text-gray-900.dark_text-gray-100= wpm
                .text-lg.text-gray-500.font-medium wpm
              .flex.flex-col.items-center.px-8
                .text-4xl.text-gray-900.dark_text-gray-100= typos
                .text-lg.text-gray-500.font-medium typos
          else
            .flex-1.h-full

          .w-64.h-full.flex.flex-col.items-center.justify-center
            button.flex.flex-col.items-center(
              className=(ready ? 'text-blue-600 dark_text-blue-400' : 'text-gray-500')
              onClick=()=>restart()
              disabled=!ready
            )
              //span.text-gray-500.pb-3 (Escape)
              .h-8
              Icon.hover_text-blue-600.dark_hover_text-blue-600.text-xl(icon='redo-alt')
              span restart

        .flex.flex-row.justify-end(style={width: 300})
          button.w-10.flex.items-center(onClick=()=>update({statsVisible: !statsVisible}))
            Icon.text-gray-600.dark_text-gray-400.text-xl.hover_text-gray-500.dark_hover_text-gray-500(icon=(statsVisible ? 'eye' : 'eye-slash'))
            .pl-2.text-gray-500 (stats)

        .flex-1.flex.items-center
          .p-2.text-4xl.tracking-wide.bg-white.dark_bg-black.rounded.border.border-gray-500.shadow-xl(
            className=(ready ? 'text-gray-900 dark_text-gray-100' : 'text-gray-600 dark_text-gray-400')
            style={width: 900}
          )
            for letter, position in exercise.content.split('')
              Letter(
                key=position
                letter=letter
                ready=ready
                cursor=cursor
                position=position
                wrong=wrong
              )

        .flex.flex-row.justify-end(style={width: 720})
          button.w-10.flex.items-center(onClick=()=>update({keyboardVisible: !keyboardVisible}))
            Icon.text-gray-600.dark_text-gray-400.text-xl.hover_text-gray-500.dark_hover_text-gray-500(icon=(keyboardVisible ? 'eye' : 'eye-slash'))
            .pl-2.text-gray-500 (keyboard)

        if keyboardVisible
          Keyboard(shift=shift keysDown=keysDown capsLock=capsLock)
        else
          div(style={height: 220})

        .flex.flex-row.justify-end.pt-8(style={width: 200})
          button.w-10.flex.items-center(onClick=()=>update({fingersVisible: !fingersVisible}))
            Icon.text-gray-600.dark_text-gray-400.text-xl.hover_text-gray-500.dark_hover_text-gray-500(icon=(fingersVisible ? 'eye' : 'eye-slash'))
            .pl-2.text-gray-500 (picture)

        if fingersVisible
          .text-center.text-gray-500.pb-8(style={height: 50}) picture of hands
        else
          .pb-8(style={height: 50})

    if showEscModal
      .fixed.inset-0.bg-black.bg-opacity-20.flex.items-center.justify-center
        .flex.flex-col.items-center.bg-gray-200.dark_bg-gray-800.text-gray-800.dark_text-gray-200.p-8.border.border-gray-800.d
          .text-xl Just keep going with the typos. It's good practice.
          .h-8

          button.border.border-blue-500.p-2.rounded.shadow(
            className=(ready ? 'text-blue-600 dark_text-blue-400' : 'text-gray-500')
            onClick=()=>update({showEscModal: false})
            disabled=!ready
          )
            span.text-lg.pr-4 I can do it
            Icon.hover_text-blue-600.dark_hover_text-blue-600.text-xl(icon='check')
  `
}

const Letter = ({letter, ready, position, cursor, wrong}) => pug`
  span(
    className=letterClassName({ready, position, cursor, wrong})
    style={
      marginLeft: '-.05em',
      marginRight: '-.05em',
      paddingLeft: '.05em',
      paddingRight: '.05em',
      borderRadius: '.075em',
    }
  )= letter
`

const letterClassName = ({ready, position, cursor, wrong}) => {
  if (!ready) return 'text-gray-600 dark_text-gray-400'
  if (cursor == position) {
    if (wrong) return 'bg-red-300 dark_bg-red-700'
    return 'bg-green-300 dark_bg-green-700'
  }
  if (cursor < position) return 'text-black dark_text-white'
  return 'text-gray-600 dark_text-gray-400'
}
