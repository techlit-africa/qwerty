import {useSounds} from 'hooks/useSounds'
import {toggleLightsOn} from 'lib/theme'
import {useFetch} from 'lib/fetch'
import {Link} from 'react-router-dom'

export const Layout = ({left, title, right, children}) => pug`
  .w-full.h-screen.flex.flex-col.bg-blue-200.dark_bg-gray-800
    .w-full.h-12.grid.grid-cols-12.border-b.border-gray-500.bg-gray-100.dark_bg-gray-900.shadow
      .col-span-3.flex.items-center= left || ''
      .col-span-6.text-center.text-3xl.font-medium.text-gray-900.dark_text-gray-100= title || ''
      .col-span-3.flex.items-center.justify-end= right || ''
    .flex-1.h-full.w-full.overflow-y-scroll= children
`

const Btn = ({children, ...props}) => pug`
  button.px-3.text-blue-800.dark_text-blue-200.hover_text-blue-600.dark_hover_text-blue-400#scrollContainer(...props)= children
`

export const TestLeft = () => pug`
  Link(to='/test')
    Btn
      Icon(icon='award')
      span.pl-3 Test
`

export const BackLeft = () => pug`
  Link(to='/lessons')
    Btn
      Icon(icon='keyboard')
      span.pl-3 Lessons
  Link(to='/stats')
    Btn
      Icon(icon='chart-bar')
      span.pl-3 Stats
`

export const VisitorRight = () => pug`
  Btn(onClick=toggleLightsOn)
    Icon.text-xl(icon='lightbulb')
`

export const UserRight = () => pug`
  - const {user, setUser} = useFetch()
  - const {muted, setMuted} = useSounds()

  Btn(onClick=()=>setMuted(!muted))
    Icon.text-xl(icon=muted ? 'volume-off' : 'volume-up')
  Btn(onClick=toggleLightsOn)
    Icon.text-xl(icon='lightbulb')
  Btn(onClick=()=>setUser(null))
    Icon(icon='user-slash')
  .text-gray-800.dark_text-gray-200.text-lg.font-mono.pr-4.truncate= user.username
`

