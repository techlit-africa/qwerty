export const Input = ({
  id,
  className,
  wrapperClassName,
  label,
  ...props
}) => pug`
  .w-full.h-full.flex.flex-col(className=wrapperClassName)
    if label
      label.text-gray-600.dark_text-gray-200= label
      .h-2
    input.px-2.py-1.text-2xl.border.border-gray-500.rounded.bg-white.dark_bg-black.dark_text-white(
      id=id
      className=className
      ...props
    )
`
