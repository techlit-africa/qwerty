export const Loading = () => pug`
  .w-full.flex.items-center.justify-center.p-12
    Icon.animate.animate-spin.text-5xl.text-gray-800.dark_text-white(icon='circle-notch')
`
