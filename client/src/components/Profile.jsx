const Profile = () => {
  const profile = {}

  const [{name, editing, saving, error}, update] =
    React.useReducer((s1, s2) => ({...s1, ...s2}), {
      name: profile?.displayName || 'Guest',
      editing: false,
      saving: false,
      error: false,
    })

  React.useEffect(
    () => update({name: profile?.displayName || 'Guest'}),
    [profile],
  )

  const saveName = async () => {
    update({saving: true})
    try {
      await fetch('/users/me', {method: 'POST', body: JSON.stringify({data: {name}})})
      mutate('/users/me')
      mutate('/users')
      update({editing: false, saving: false})
    } catch (e) {
      update({saving: false, error: true})
    }
  }

  const submitIfEnter = (e) => {
    if (e.keyCode == 13) saveName()
  }

  return pug`
    if editing
      .px-1
        button.w-10.h-10.bg-green-600.rounded-full(onClick=()=>{})
          Icon.text-xl.text-white(icon='user')
      input.py-1.px-3.bg-gray-100.dark_bg-gray-600.border.border-gray-200.dark_border-gray-500.dark_text-white.text-lg.rounded-lg(
        value=name
        onChange=(e)=>update({name: e.target.value})
        placeholder='Your name'
        onKeyUp=submitIfEnter
        autoFocus
      )
      button.px-3(onClick=saveName)
        Icon.text-2xl.text-green-600(icon='check')

    else
      .px-1
        button.w-10.h-10.bg-green-600.rounded-full(onClick=()=>{})
          Icon.text-xl.text-white(icon='user')
      .flex.items-center(onClick=()=>update({editing: true}))
        input.py-1.px-3.bg-gray-100.dark_bg-gray-700.border.border-gray-200.dark_border-gray-600.dark_text-white.text-lg.rounded-lg.cursor-pointer(
          defaultValue=name
          placeholder='Your name'
          disabled
        )
        button.px-3
          Icon.text-xl.text-gray-600.dark_text-gray-400(icon='pen')
  `
}
