import {Loading} from 'components/Loading'
import {Failed} from 'components/Failed'
import {HomeLayout} from 'components/HomeLayout'
import {Link} from 'react-router-dom'
import useSWR from 'swr'

export const Lessons = () => pug`
  HomeLayout
    ErrorBoundary(FallbackComponent=Failed)
      React.Suspense(fallback=${pug`Loading`})
        Ready
`

const useAccordion = (lessons) => {
  const [openId, setOpenId] = React.useState(() => 0)

  React.useEffect(() => {
    const newOpenId = lessons.filter((l) => l.available).slice(-1)[0].id
    setOpenId(newOpenId)

    const container = document.getElementById('scrollContainer')
    const el = document.getElementById('accordion-'+newOpenId)
  }, [lessons])

  return [openId, setOpenId]
}

const Ready = () => pug`
  - const {data: lessons} = useSWR('/lessons')
  - const [openId, setOpenId] = useAccordion(lessons)

  for lesson in lessons
    - const isOpen = lesson.id === openId
    .pt-2.pb-4.mb-2.border-b.border-gray-300.dark_border-gray-700(key=lesson.id id='accordion-'+lesson.id)
      .flex.items-center
        .w-72.text-xl.text-gray-600.dark_text-gray-400 #{lesson.position + 1}. #{lesson.title}
        .flex-1.flex.justify-end
          if lesson.accurate && lesson.fast
            = badge('star', true)
          else
            .pr-3= badge('check', lesson.complete)
            .pr-3= badge('bullseye', lesson.accurate)
            .pr-3= badge('bolt', lesson.fast)

        button.w-12(
          disabled=!lesson.available
          className=chevronClassName(lesson.available)
          onClick=()=>setOpenId(isOpen ? '' : lesson.id)
        )
          Icon.text-2xl(icon=isOpen ? 'chevron-up' : 'chevron-down')

      if isOpen
        .grid.grid-cols-3.gap-4.py-4
          for exercise in lesson.exercises
            Link(
              to=(exercise.available ? '/exercises/'+exercise.id : '#')
              key=exercise.id
            )
              .h-full.p-2.mt-2.border.border-gray-300.dark_border-gray-800.shadow.rounded.text-center(
                className=exerciseClassName(exercise.available)
              )
                .text-xl #{exercise.position + 1}. #{exercise.title}
                .flex.justify-center
                  if exercise.accurate && exercise.fast
                    = bigBadge('star', true)
                  else
                    .pr-3= bigBadge('check',    exercise.complete)
                    .pr-3= bigBadge('bullseye', exercise.accurate)
                    .pr-0= bigBadge('bolt',     exercise.fast)
                .text-xs.text-gray-400.dark_text-gray-600.truncate #{exercise.content}
`

const badge = (icon, active) => pug`
  Icon.text-xl(className=badgeClassName(active) icon=icon)
`

const bigBadge = (icon, active) => pug`
  Icon.text-4xl.my-4(className=badgeClassName(active) icon=icon)
`

const badgeClassName = active =>
  active ? 'text-yellow-500' : 'text-gray-400 dark_text-gray-600'

const chevronClassName = active =>
  active
    ? 'text-blue-600 dark_text-blue-400 hover_text-blue-400 dark_hover_text-blue-600'
    : 'text-gray-500'

const exerciseClassName = active =>
  active
    ? (' bg-gray-50 dark_bg-black text-blue-600 dark_text-blue-400 ' +
      ' hover_bg-white dark_hover_border-blue-800 active_bg-gray-100 ' +
      ' dark_active_bg-gray-900 ')
    : 'text-gray-500'
