import {useKeysDown} from 'hooks/useKeysDown'
import {useSounds} from 'hooks/useSounds'
import {Keyboard} from 'components/Keyboard'
import {Layout, BackLeft, UserRight} from 'components/Layout'
import {useFetch} from 'lib/fetch'
import {testLines} from 'testLines'
import {Link, useHistory} from 'react-router-dom'

const randomTest = () => {
  const uniq = {}
  while (Object.keys(uniq).length < 2) {
    uniq[testLines.length * Math.random() | 0] = true
  }
  return Object.keys(uniq).map((i) => testLines[i]).join(' ')
}

const testInit = {
  wpm: 0,
  typos: 0,
  accuracy: 1,
  wrong: false,
  cursor: 0,
  ready: false,
  startedAt: false,
}

export const Test = () => {
  const history = useHistory()
  const {fetch} = useFetch()

  const [{
    content,
    wpm,
    typos,
    accuracy,
    wrong,
    cursor,
    ready,
    startedAt,
    stoppedAt,
    statsVisible,
    showEscModal,
  }, reducer] =
    React.useReducer(
      (state, {type, data}) => ({
        update: (newState) => ({...state, ...newState}),
        wpmTick: () => {
          if (!state.startedAt || state.stoppedAt) return state
          const words = (state.cursor + 1) / 5
          const minutes = (new Date() - state.startedAt) / 1000 / 60
          return {
            ...state,
            wpm: Math.round(words / minutes),
            accuracy: (state.cursor - state.typos) / state.cursor,
          }
        },
      })[type]?.(data) || state,
      {
        ...testInit,
        statsVisible: true,
        showEscModal: false,
        content: randomTest(),
      },
    )

  const update = (s2) => reducer({type: 'update', data: s2})

  const {
    playOops,
    playKeyUp,
    playKeyDown,
    playStart,
    playEnd,
  } = useSounds()

  const restart = () => {
    playStart?.()
    update(testInit)

    // TODO: add a UI effect that makes a 1000ms timeout less surprising
    setTimeout(() => update({ready: true}), 10)
  }

  React.useEffect(() => {
    restart()
    const wpmTick = setInterval(() => reducer({type: 'wpmTick'}), 300)
    return () => clearInterval(wpmTick)
  }, [])

  const keyDownRef = React.useRef()
  const keyUpRef = React.useRef()

  keyDownRef.current = (event) => {
    if (!ready || event.ctrlKey || event.altKey) return

    if (
      event.code.startsWith('Key') ||
      event.code.startsWith('Digit') ||
      [
        'Backquote',
        'Space',
        'Backspace',
        'Backslash',
        'BracketRight',
        'BracketLeft',
        'Quote',
        'Semicolon',
        'Slash',
        'Period',
        'Comma',
      ].includes(event.code)
    ) {
      event.stopPropagation()
      event.preventDefault()
    }

    playKeyDown()

    if (event.code === 'Escape') {
      event.preventDefault()
      update({showEscModal: !showEscModal})
      return
    }

    if (stoppedAt) return

    if (event.key === content[cursor]) {
      update({
        cursor: cursor + 1,
        wrong: false,
      })

      if (!startedAt) update({startedAt: new Date()})

      if (cursor >= content.length - 1) {
        update({stoppedAt: new Date()})

        playEnd()

        const accuracy = (content.length - typos) / content.length

        fetch('/test_submissions', {
          method: 'POST',
          body: JSON.stringify({content, typos, wpm, accuracy}),
        })
          .then(({error, ...submission}) => {
            if (error) {
              console.error(error)
              history.push(`/test`)
            }
            history.push(`/test-submissions/${submission.id}`)
          })
          .catch((error) => {
            console.error(error)
            history.push(`/test`)
          })
      }
      return
    }

    if (
      event.ctrlKey ||
      event.altKey ||
      ['ShiftLeft', 'ShiftRight', 'CapsLock'].includes(event.code)
    ) return

    update({wrong: true, typos: typos + 1})
    playOops()
  }

  keyUpRef.current = (event) => {
    playKeyUp()
  }

  const [shift, capsLock, keysDown] = useKeysDown({
    onKeyDownRef: keyDownRef,
    onKeyUpRef: keyUpRef,
  })

  return pug`
    Layout(left=${pug`BackLeft`} title='Touchtyping Test')
      .w-full.flex-1.flex.flex-col.items-center.justify-start
        .w-full.max-w-screen-lg.mx-auto.h-24.my-8.flex.justify-between
          .h-full.flex.items-center
            .flex.flex-col.items-center.px-8
              .text-4xl.text-gray-900.dark_text-gray-100= wpm
              .text-lg.text-gray-500.font-medium wpm
            .flex.flex-col.items-center.px-8
              .flex.items-end.text-gray-900.dark_text-gray-100
                .text-4xl= Math.round(accuracy * 100)
                .pl-1.text-2xl %
              .text-lg.text-gray-500.font-medium accuracy

        .p-2.text-2xl.tracking-wide.bg-white.dark_bg-black.rounded.border.border-gray-500.shadow-xl(
          className=(ready ? 'text-gray-900 dark_text-gray-100' : 'text-gray-600 dark_text-gray-400')
          style={width: 900}
        )
          for letter, position in content.split('')
            Letter(
              key=position
              letter=letter
              ready=ready
              cursor=cursor
              position=position
              wrong=wrong
            )

    if showEscModal
      .fixed.inset-0.bg-black.bg-opacity-20.flex.items-center.justify-center
        .flex.flex-col.items-center.bg-gray-200.dark_bg-gray-800.text-gray-800.dark_text-gray-200.p-8.border.border-gray-800.d
          .text-xl Just keep going with the typos. It's good practice.
          .h-8

          button.border.border-blue-500.p-2.rounded.shadow(
            className=(ready ? 'text-blue-600 dark_text-blue-400' : 'text-gray-500')
            onClick=()=>update({showEscModal: false})
            disabled=!ready
          )
            span.text-lg.pr-4 I can do it
            Icon.hover_text-blue-600.dark_hover_text-blue-600.text-xl(icon='check')
  `
}

const Letter = ({letter, ready, position, cursor, wrong}) => pug`
  span(
    className=letterClassName({ready, position, cursor, wrong})
    style={
      marginLeft: '-.05em',
      marginRight: '-.05em',
      paddingLeft: '.05em',
      paddingRight: '.05em',
      borderRadius: '.075em',
    }
  )= letter
`

const letterClassName = ({ready, position, cursor, wrong}) => {
  if (!ready) return 'text-gray-600 dark_text-gray-400'
  if (cursor == position) {
    if (wrong) return 'bg-red-300 dark_bg-red-700'
    return 'bg-green-300 dark_bg-green-700'
  }
  if (cursor < position) return 'text-black dark_text-white'
  return 'text-gray-600 dark_text-gray-400'
}
