import {Layout, BackLeft} from 'components/Layout'
import {Loading} from 'components/Loading'
import {Failed} from 'components/Failed'
import {useKeysDown} from 'hooks/useKeysDown'
import {Link, useParams, useHistory} from 'react-router-dom'
import useSwr from 'swr'

export const TestSubmission = () => pug`
  ErrorBoundary(FallbackComponent=Failed)
    React.Suspense(fallback=${pug`Loading`})
      Ready
`

const Ready = () => {
  const {id} = useParams()
  const history = useHistory()
  const {data: submission} = useSwr(id ? `/test_submissions/${id}` : null)

  const keyDownRef = React.useRef()
  keyDownRef.current = (event) => {
    if (event.code === 'Escape') {
      history.push('/test')
    }
    if (event.code === 'Enter') {
      history.push('/stats')
    }
  }

  useKeysDown({
    onKeyDownRef: keyDownRef,
  })

  return pug`
    Layout(left=${pug`BackLeft`} title='Touchtyping Test')
      .w-full.flex-1.flex.flex-col.overflow-y-scroll.items-center.p-8
        .w-full.max-w-screen-md.flex.flex-col.items-center.py-4.border.border-gray-500.bg-gray-100.dark_bg-gray-900.shadow.rounded

          .w-full.text-center.pt-8.pb-2.text-4xl.text-gray-900.dark_text-gray-100 Test Results

          .w-full.flex.justify-between
            Link(to='/test')
              button.w-48.flex.flex-col.items-center.text-blue-800.dark_text-blue-200.hover_text-blue-700.dark_hover_text-blue-300.active_text-blue-800.dark_active_text-blue-200.font-medium
                Icon.text-4xl(icon='arrow-alt-circle-left')
                span.text-2xl Try again
                span.text-gray-500 (Escape)

            .flex.items-center.justify-center.pt-8.pb-12
              Icon.text-9xl.text-yellow-500(icon='award')

            Link(to='/stats')
              button.w-48.flex.flex-col.items-center.text-blue-800.dark_text-blue-200.hover_text-blue-700.dark_hover_text-blue-300.active_text-blue-800.dark_active_text-blue-200.font-medium
                Icon.text-4xl(icon='arrow-alt-circle-right')
                span.text-2xl Continue
                span.text-gray-500 (Enter)

          .w-72.flex.items-end.justify-between
            .flex.flex-col.items-center
              .text-6xl.text-gray-900.dark_text-gray-100= submission.wpm
              .text-2xl.text-gray-500.font-medium wpm
            .flex.flex-col.items-center
              .text-6xl.text-gray-900.dark_text-gray-100 #{Math.round(submission.accuracy * 100)}%
              .text-2xl.text-gray-500.font-medium accuracy
  `
}
