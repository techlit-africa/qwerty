export const Failed = () => pug`
  .w-full.flex.items-center.justify-center.p-12
    Icon.text-5xl.text-red-500.dark_text-white(icon='exclamation-triangle')
`
