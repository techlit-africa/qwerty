import {Switch, Route, Redirect} from 'react-router-dom'
import {Login} from 'components/Login'
import {Test} from 'components/Test'
import {TestSubmission} from 'components/TestSubmission'
import {Exercise} from 'components/Exercise'
import {Submission} from 'components/Submission'
import {Layout} from 'components/Layout'
import {Lessons} from 'components/Lessons'
import {Stats} from 'components/Stats'
import {Tips} from 'components/Tips'
import {useFetch} from 'lib/fetch'

const UserRoute = ({path, children}) => pug`
  - const {user} = useFetch()
  Route(path=path)
    if user
      = children
    else
      Redirect(to='/login')
`

const VisitorRoute = ({path, children}) => pug`
  - const {user} = useFetch()
  Route(path=path)
    if user
      Redirect(to='/stats')
    else
      = children
`

export const App = () => pug`
  Switch
    VisitorRoute(path='/login') #[Login]
    UserRoute(path='/lessons') #[Lessons]
    UserRoute(path='/stats') #[Stats]
    UserRoute(path='/tips') #[Tips]
    UserRoute(path='/exercises/:id') #[Exercise]
    UserRoute(path='/submissions/:id') #[Submission]
    UserRoute(path='/test-submissions/:id') #[TestSubmission]
    UserRoute(path='/test') #[Test]
    Route #[Redirect(to='/lessons')]
`
