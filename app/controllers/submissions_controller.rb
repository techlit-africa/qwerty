class SubmissionsController < ApplicationController
  before_action :authorize_request
  before_action :set_submission, only: %i[show]

  def create
    @exercise = Exercise.find(params[:exercise_id])
    complete = params[:typos] <= @exercise.okay_typos && params[:wpm] >= @exercise.okay_wpm
    accurate = complete && params[:typos] <= @exercise.good_typos
    fast = complete && params[:wpm] >= @exercise.good_wpm

    @submission = Submission.create \
      **submission_params,
      user_id: @current_user.id,
      complete: complete,
      accurate: accurate,
      fast: fast

    if @submission.persisted?
      show
    else
      json = {error: @submission.errors.full_messages.to_sentence}
      render json: json, status: :unprocessable_entity
    end
  end

  def show
    @exercise = Exercise.all_by_id[@submission.exercise_id]
    @lesson = Lesson.all_by_id[@exercise.lesson_id]

    render json: {
      **@submission.attributes,
      lesson: @lesson,
      exercise: @exercise,
      next_exercise_id: Exercise.next_exercise_id(@lesson.id, @exercise.id),
    }
  end

  private

  def set_submission
    @submission = Submission.find(params[:id])
  end

  def submission_params
    params.permit(:exercise_id, :wpm, :typos, :accuracy)
  end
end
