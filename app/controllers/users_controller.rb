class UsersController < ApplicationController
  before_action :authorize_request, only: :update

  def index
    render json: User.all
  end

  def create
    if @user = User.where(username: params[:username]).first
      if @user.authenticate(params[:password])
        render json: {token: JsonWebToken.encode(user_id: @user.id), **@user.attributes}
      else
        render json: {error: 'This username is taken.'}, status: :unauthorized
      end
    else
      @user = User.create(user_params)
      if @user.persisted?
        render json: {token: JsonWebToken.encode(user_id: @user.id), **@user.attributes}
      else
        render json: {error: @user.errors.full_messages.to_sentence}, status: :unprocessable_entity
      end
    end
  end

  def update
    if @current_user.update(user_params)
      render json: @current_user.reload
    else
      render json: {error: @current_user.errors.full_messages.to_sentence}, status: :unprocessable_entity
    end
  end

  private

  def user_params
    params.permit(:klass, :username, :password)
  end
end
