class TestSubmissionsController < ApplicationController
  before_action :authorize_request
  before_action :set_test_submission, only: %i[show]

  def create
    @test_submission =
      TestSubmission.create(**test_submission_params, user_id: @current_user.id)
    if @test_submission.persisted?
      show
    else
      json = {error: @test_submission.errors.full_messages.to_sentence}
      render json: json, status: :unprocessable_entity
    end
  end

  def show
    render json: @test_submission
  end

  private

  def set_test_submission
    @test_submission = TestSubmission.find(params[:id])
  end

  def test_submission_params
    params.permit(:content, :wpm, :typos, :accuracy)
  end
end
