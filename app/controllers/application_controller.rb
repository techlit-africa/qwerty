class ApplicationController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound, with: :not_found
  def not_found
    render json: {error: 'API route not found.'}, status: :not_found
  end

  def client_html
    render file: 'public/index.html'
  end

  protected

  def authorize_request
    header = request.headers['Authorization']
    header = header.split(' ').last if header
    begin
      @decoded = JsonWebToken.decode(header)
      @current_user = User.find(@decoded[:user_id])
    rescue ActiveRecord::RecordNotFound => e
      render json: {error: 'Wrong username. Does it exist?'}, status: :unauthorized
    rescue JWT::DecodeError => e
      render json: {error: 'Invalid token. Are you logged in?'}, status: :unauthorized
    end
  end
end
