class SessionsController < ApplicationController
  def create
    unless @user = User.where(username: params[:username]).first
      render json: {error: "Wrong username. Is it typed correctly?"}, status: :not_found
    else
      if @user.authenticate(params[:password])
        render json: {token: JsonWebToken.encode(user_id: @user.id), **@user.attributes}
      else
        render json: {error: 'Wrong password. Is this your account?'}, status: :unauthorized
      end
    end
  end
end
