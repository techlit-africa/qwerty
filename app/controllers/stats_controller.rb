class StatsController < ApplicationController
  before_action :authorize_request

  def index
    @stats = Stats.new
    render json: @stats.global
  end
end
