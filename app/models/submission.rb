class Submission < ApplicationRecord
  validates_presence_of :wpm, :typos, :accuracy
  validates :complete, :accurate, :fast, inclusion: [true, false]
  has_one :course, through: :lesson
  has_one :lesson, through: :exercise
  belongs_to :exercise
  belongs_to :user
end
