class Lesson < ApplicationRecord
  validates_presence_of :title, :cuid, :position
  belongs_to :course
  has_many :exercises
  has_many :submissions, through: :exercises
  has_many :users, through: :submissions

  def self.all_by_id
    @all_by_id ||= all.group_by(&:id).transform_values(&:first)
  end

  def self.for_home_page
    @for_home_page ||= order(:position).map do |lesson|
      lesson.attributes.symbolize_keys.merge \
        exercises: Exercise.all_by_lesson_id[lesson.id].map { _1.attributes.symbolize_keys }
    end
  end

  def self.first_id
    for_home_page.dig(0, :id)
  end
end
