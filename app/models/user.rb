class User < ApplicationRecord
  has_secure_password
  validates :klass, presence: true
  validates :username, presence: true, uniqueness: true, length: { minimum: 4 }

  has_many :submissions
  has_many :exercises, through: :submissions
  has_many :lessons, through: :exercises
  has_many :courses, through: :courses
end
