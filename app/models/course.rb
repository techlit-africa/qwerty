class Course < ApplicationRecord
  validates_presence_of :title, :cuid, :version
  has_many :lessons
  has_many :exercises, through: :lessons
  has_many :submissions, through: :exercises
  has_many :users, through: :submissions
end
