class Klass < ApplicationRecord
  validates :klass, presence: true, uniqueness: true
end
