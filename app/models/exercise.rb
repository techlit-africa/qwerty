class Exercise < ApplicationRecord
  validates_presence_of :title, :cuid, :position, :content, :okay_wpm, :okay_typos, :good_typos, :good_wpm
  has_one :course, through: :lesson
  belongs_to :lesson
  has_many :submissions
  has_many :users, through: :submissions

  def self.all_by_id
    @all_by_id ||= all.group_by(&:id).transform_values(&:first)
  end

  def self.all_by_lesson_id
    @by_lesson_id ||= all.order(:position).group_by(&:lesson_id)
  end

  def self.first_id
    Lesson.for_home_page.dig(0, :exercises, 0, :id)
  end

  def self.next_exercise_id(lesson_id, exercise_id)
    lesson = Lesson.all_by_id[lesson_id]
    exercise = all_by_id[exercise_id]

    Exercise.all_by_lesson_id.dig(lesson_id, exercise.position + 1)&.id ||
      Lesson.for_home_page.dig(lesson.position + 1, :exercises, 0, :id)
  end

  def self.next_positions(lesson_position, exercise_position)
    if Lesson.for_home_page.dig(lesson_position, :exercises, exercise_position + 1)
      [lesson_position, exercise_position + 1]
    elsif Lesson.for_home_page.dig(lesson_position + 1, :exercises, 0)
      [lesson_position + 1, 0]
    else
      []
    end
  end
end
