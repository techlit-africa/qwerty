class TestSubmission < ApplicationRecord
  validates_presence_of :content, :wpm, :typos, :accuracy
end
