# README

## System Requirements

### 0. `asdf`

`asdf` is a version manager. You don't need it in order to
run the app, but the rest of the instructions will depend on
it. If you have your own preferred version managers, then
you can translate the instructions.

Here are instructions to do install `asdf`, assuming you're
using `bash`:
```
git clone https://github.com/asdf-vm/asdf ~/.asdf
echo '. ~/.asdf/asdf.sh' >> ~/.bashrc
. ~/.bashrc
```

### 1. `ruby` & `bundler`

You'll need `ruby` version `2.7.0` and the `bundler` gem
installed.

The app dependencies may depend on other system libraries,
which you'll have to figure out on your own.

```
asdf plugin add ruby
asdf install ruby 2.7.0
asdf global ruby 2.7.0
gem install bundler
```

### 2. `node` & `yarn`

```
asdf plugin add nodejs
asdf install nodejs 10.9.0
asdf global nodejs 10.9.0
npm install --global yarn
```

## Setup

1. Get the project
2. Install ruby dependencies
3. Install nodejs dependencies
4. Setup the database
```
git clone https://gitlab.com/techlit-africa/qwerty
cd qwerty
bundle install
(cd client && yarn install)
bundle exec rails db:{create,migrate,seed}
```

## Running

```
./bin/dev
```
